/* globals modeles.js/  state
           modeles.js/  getQuizzes ; getUser ; getQuizEtQuestion ; getMyQuizzes ; getMyAnswers 
           modification.js/  creationQuiz ; addQuestionToQuiz ; deleteQuestion
           ajout_suppresion_quiz.js/ ;  envoyer ; modifyQuiz ; modifyQuestion 
           formulaire.js/   clickAdd ; clickAddQuestion ; clickModifyQuiz ; clickModifyQuestion*/


// //////////////////////////////////////////////////////////////////////////////
// HTML : fonctions génération de HTML à partir des données passées en paramètre
// //////////////////////////////////////////////////////////////////////////////

// génération d'une liste de quizzes avec deux boutons en bas
const htmlQuizzesList = (quizzes, curr, total) => {
  console.debug(`@htmlQuizzesList(.., ${curr}, ${total})`);


  // un élement <li></li> pour chaque quizz. Noter qu'on fixe deux données
  // data-quizzid qui sera accessible en JS via element.dataset.quizzid.
  // data-main qui sera aussi accessible en JS via element.dataset.main
  // On définit aussi .modal-trigger et data-target="id-modal-quizz-menu"
  // pour qu'une fenêtre modale soit affichée quand on clique dessus
  // VOIR https://materializecss.com/modals.html
  const quizzesList = quizzes.map(
    (q) =>
      `<li class="collection-item modal-trigger red lighten-5" data-target="id-modal-quizz-menu" data-quizzid="${q.quiz_id}" data-main="id-all-quizzes-main">
        <h5>${q.title}</h5>
        <p>${q.description}</p>
        <p>${q.questions_number} question(s)</p>  
        <p>Créer le ${q.created_at} par <a class="chip">${q.owner_id}</a></p>
      </li>`
  );


  // le bouton "<" pour revenir à la page précédente, ou rien si c'est la première page
  // on fixe une donnée data-page pour savoir où aller via JS via element.dataset.page
  const prevBtn =
    curr !== 1 && curr !== 0
      ? `<button id="id-prev-quizzes" data-page="${curr -
          1}" class="btn"><i class="material-icons">navigate_before</i></button>`
      : '';


  // le bouton ">" pour aller à la page suivante, ou rien si c'est la première page
  const nextBtn =
    curr !== total
      ? `<button id="id-next-quizzes" data-page="${curr +
          1}" class="btn"><i class="material-icons">navigate_next</i></button>`
      : '';


  // La liste complète et les deux boutons en bas
  const html = `
  <ul class="collection">
    ${quizzesList.join('')}
  </ul>
  <div class="row">      
    <div class="col s6 left-align">${prevBtn}</div>
    <div class="col s6 right-align">${nextBtn}</div>
  </div>
  `;


  return html;
};


// Génération de la liste de MesQuiz
const htmlMyQuizzesList = (quizzes) => {
  console.debug(`@htmlMyQuizzesList()`);


  // un élement <li></li> pour chaque quizz. Noter qu'on fixe deux données
  // data-quizzid qui sera accessible en JS via element.dataset.quizzid.
  // data-main qui sera aussi accessible en JS via element.dataset.main
  // On définit aussi .modal-trigger et data-target="id-modal-quizz-menu"
  // pour qu'une fenêtre modale soit affichée quand on clique dessus
  // VOIR https://materializecss.com/modals.html
  const quizzesList = quizzes.map(
    (q) =>

      `<!-- Bouton qui appel le formulaire de modification d'un quiz (titre et description) --> 
      <a class="waves-effect waves-light btn-small right green" onclick=clickModifyQuiz(${q.quiz_id})>
          <i class="material-icons">border_color</i>
        </a>

        <li class="collection-item modal-trigger green lighten-5" data-target="id-modal-quizz-menu" data-quizzid="${q.quiz_id}" data-main="id-all-myquizzes-main">
          <h5>${q.title}</h5>
          <p>${q.description}</p>
          <p>${q.questions_number} question(s)</p>  
          <p>Créer le ${q.created_at} par <a class="chip">${q.owner_id}</a></p>
        </li>`
  );


  // Bouton qui servira pour l'ajout d'un quiz
  const buttonAdd = `<a class="btn-small waves-effect waves-light blue" onclick=clickAdd()>
                      <i class="material-icons">add</i>
                    </a>`


  // La liste complète avec un bouton situé au-dessus de la liste 
  // qui permettra d'ajouter un quiz 
  const html = `
  <ul class="collection">
    ${quizzesList.join('')}
  </ul>
  <p>${buttonAdd}</p>`;
  

  return html;
};


// Génération de la liste de MesQuiz
const htmlMyAnswersList = (quizzes) => {
  console.debug(`@htmlMyAnswersList`);


  // un élement <li></li> pour chaque quizz. Noter qu'on fixe deux données
  // data-quizzid qui sera accessible en JS via element.dataset.quizzid.
  // data-main qui sera aussi accessible en JS via element.dataset.main
  // On définit aussi .modal-trigger et data-target="id-modal-quizz-menu"
  // pour qu'une fenêtre modale soit affichée quand on clique dessus
  // VOIR https://materializecss.com/modals.html
  const quizzesList = quizzes
   .map(
    (q) =>

      `<li class="collection-item modal-trigger orange lighten-5" data-target="id-modal-quizz-menu" data-quizzid="${q.quiz_id}" data-main="id-all-myanswers-main">
         <h5>${q.title}</h5>
        <p>${q.description}</p>
        <p>${q.answers.length} réponse(s) donnée(s)<p>
        <p>Créer le ${q.created_at} par <a class="chip">${q.owner_id}</a></p>
        </br>
    </li>`)
  .join('');


  // La liste complète
  const html = `
  <ul class="collection">
  ${quizzesList}    
  </ul>`;
  

  return html;
};


// //////////////////////////////////////////////////////////////////////////////
// RENDUS : mise en place du HTML dans le DOM et association des événemets
// //////////////////////////////////////////////////////////////////////////////

// met la liste HTML dans le DOM et associe les handlers aux événements
// eslint-disable-next-line no-unused-vars
function renderQuizzes() {// Render pour l'onglet TOUS LES QUIZ
  console.debug(`@renderQuizzes()`);


  // Les éléments à mettre à jour : le conteneur pour la liste des quizz
  const usersElt = document.getElementById('id-all-quizzes-list');


  // On appelle la fonction de généraion et on met le HTML produit dans le DOM
  usersElt.innerHTML = htmlQuizzesList(
    state.quizzes.results,
    state.quizzes.currentPage,
    state.quizzes.nbPages
  );


  // /!\ il faut que l'affectation usersElt.innerHTML = ... ait eu lieu pour
  // /!\ que prevBtn, nextBtn et quizzes en soient pas null
  // les éléments à mettre à jour : les boutons
  const prevBtn = document.getElementById('id-prev-quizzes');
  const nextBtn = document.getElementById('id-next-quizzes');


  // la liste de tous les quizzes individuels
  const quizzes = document.querySelectorAll('#id-all-quizzes-list li');


  // les handlers quand on clique sur "<" ou ">"
  function clickBtnPager() {
    // remet à jour les données de state en demandant la page
    // identifiée dans l'attribut data-page
    // noter ici le 'this' QUI FAIT AUTOMATIQUEMENT REFERENCE
    // A L'ELEMENT AUQUEL ON ATTACHE CE HANDLER
    getQuizzes(this.dataset.page);
  } 


  if (prevBtn) prevBtn.onclick = clickBtnPager;
  if (nextBtn) nextBtn.onclick = clickBtnPager;


  // pour chaque quizz, on lui associe son handler
  quizzes.forEach((q) => {
    q.onclick = clickQuiz;
  });
  // qd on clique sur un quizz, on change sont contenu avant affichage
  // l'affichage sera automatiquement déclenché par materializecss car on
  // a définit .modal-trigger et data-target="id-modal-quizz-menu" dans le HTML
}


// Met la liste HTML dans le DOM et associe les handlers aux événements
// eslint-disable-next-line no-unused-vars
function renderMyQuizzes() {// Render pour l'onglet MES QUIZ
  console.debug(`@renderMyQuizzes()`);


  // les éléments à mettre à jour : le conteneur pour la liste des quizz
  const usersElt = document.getElementById('id-all-myquizzes-list');


  // On appelle la fonction de généraion et on met le HTML produit dans le DOM
  usersElt.innerHTML = htmlMyQuizzesList(state.myquizzes);


  // La liste de tous les quizzes individuels
  const myquizzes = document.querySelectorAll("#id-all-myquizzes-list li");


  // Pour chaque quizz, on lui associe son handler
   myquizzes.forEach((q) => {
     q.onclick = clickQuiz;
   });
  // qd on clique sur un quizz, on change sont contenu avant affichage
  // l'affichage sera automatiquement déclenché par materializecss car on
  // a définit .modal-trigger et data-target="id-modal-quizz-menu" dans le HTML
}


// Met la liste HTML dans le DOM et associe les handlers aux événements
// eslint-disable-next-line no-unused-vars
function renderMyAnswers() {// Render pour l'onglet MES REPONSES
  console.debug(`@renderMyAnswers()`);


  // Les éléments à mettre à jour : le conteneur pour la liste des quizz
  const usersElt = document.getElementById('id-all-myanswers-list');


  // On appelle la fonction de génération et on met le HTML produit dans le DOM
  usersElt.innerHTML = htmlMyAnswersList(state.myanswers);


  // La liste de tous les quizzes individuels
  const myanswers = document.querySelectorAll("#id-all-myanswers-list li");


  // Pour chaque quizz, on lui associe son handler
   myanswers.forEach((q) => {
     q.onclick = clickQuiz;
   });
   // qd on clique sur un quizz, on change sont contenu avant affichage
  // l'affichage sera automatiquement déclenché par materializecss car on
  // a définit .modal-trigger et data-target="id-modal-quizz-menu" dans le HTML
}


// Affiche une prévisualisation du contenu du quiz sur lequel on a cliqué
function renderPreviewQuiz(){
  console.debug(`@renderPreviewQuiz()`);


  // Une fenêtre modale définie dans le HTML
  const modal = document.getElementById('id-modal-quizz-menu');


  // On affiche un aperçus des questions du quiz
  const questionList = state.questions
  .map(
    (q) =>`
      <li class="collection-item cyan lighten-4">
        <h5>${q.sentence}</h5>
        <ol>${q.propositions.map((q1) =>`<li>${q1.content}</li>`).join('')}</ol>
      </li>`
    )
    .join('');


    // Génération du code HTML
    const html = 
      `<h5>Questions :</h5>
        <ol class="collection">
        ${questionList}
        </ol>`;

    
    // On y affiche dans un modal définie dans le HTML
    modal.children[0].innerHTML = html;
}


// On affiche le quiz 
function renderQuiz() {
  console.debug(`@renderQuiz()`);


  // On récupère le main où sera afficher les informations du quiz
  const main = document.getElementById(state.main);  


  // On créer une liste composé des id des quiz créer par l'utilisateur
  const list = state.myquizzes.map(x => x.quiz_id);


  // On affiche le quiz et les propositions
  // Si l'utilisateur a déja répondu au quiz affiché
  // Les réponses qu'il avait donné seront écrite en verte et pré-séléctionné
  // Si l'utilisateur n'est pas connecté, il ne pourra pas répondre au quiz (On l'informe)
  const questionList = state.questions
    .map(
    (q1) =>`
      <li class="collection-item grey lighten-4">
        <h5>
          ${q1.sentence}

          <! -- Bouton pour modifier une question uniquement pour les quizzes de l'utilisateur -->
          ${(list.some(x => x == state.currentQuizz) && state.main === "id-all-myquizzes-main")              
          ?`<a class="waves-effect waves-light btn-small green" onclick="clickModifyQuestion(${q1.question_id})">
              <i class="material-icons">border_color</i>
          </a>`
          : ""}

          <! -- Bouton pour supprimer une question uniquement pour les quizzes de l'utilisateur -->
          ${(list.some(x => x == state.currentQuizz) && state.main === "id-all-myquizzes-main")              
          ?`<button class="btn-floating waves-effect modal-trigger waves-light right red" data-target="id-modal-login" type="button" name="deleteQ" onclick="deleteQuestionHandler(${q1.question_id})">
              <i class="material-icons">delete</i>
          </button>`
          : ""}
        </h5>

        <ol>
          ${q1.propositions.map((q2) =>` 
            <li>   
              <p>
                <label>
                    <input 
                        ${(state.quiz.open && !list.some(x => x == state.currentQuizz) && state.xApiKey !== "")?``:`disabled`} 
                        type="radio" id="choix${q1.question_id}${q2.proposition_id}" 
                        name="${q1.question_id}" 
                        value="${q2.proposition_id}" onchange="envoyer()"
                        ${propositionsUser(q1.question_id,q2.proposition_id)?"checked":""}
                      />
                    <span ${propositionsUser(q1.question_id,q2.proposition_id)?'style=color:green;':'style=color:black'}>${q2.content}</span>
                </label>
              </p>
            </li>`)
          .join('')}
      </ol>
    </li>`)
  .join('');
  

  // Bouton qui permet d'ajouter une question
  // On ne l'affiche que pour les quiz créer par l'utilisateur
  const buttonAdd =
  (list.some(x => x == state.currentQuizz) && state.main === "id-all-myquizzes-main")
  ?`<a class="waves-effect waves-light btn-small blue left" onclick= clickAddQuestion()>
      <i class="material-icons">playlist_add</i>
    </a>`
  :'';


  // On affiche le tout
  main.innerHTML =`
  <form name="formQuestions">
    <ul class="collection">
      <li class="collection-item grey lighten-4">
        <p>${list.some(x => x == state.currentQuizz)?"Le quiz vous appartient":""}</p>
        <p>Quiz : ${state.quiz.open?"Ouvert":"Fermer"}</p>
        <p style="color:red">${state.xApiKey !== ""?"":`<i class="material-icons">warning</i> Vous devez vous connecter pour répondre !`}</p>
      </li>
      ${questionList}
    </ul>
    ${buttonAdd}
  </form>
  `;
}


// //////////////////////////////////////////////////////////////////////////////
// AUTRES 
// //////////////////////////////////////////////////////////////////////////////


// Fonction qui gére les clicks sur les quiz
function clickQuiz() {
  const modal = document.getElementById('id-modal-quizz-menu');

  // Si les questions prennent du temps à charger
  // On indique à l'utilisateur qu'un chargement s'effectue
  modal.children[0].innerHTML =`
    <div class="progress">
      <div class="indeterminate"></div>
  </div>`


  // On récupère l'id du quiz sélectionné
  state.currentQuizz = this.dataset.quizzid;


  // Si on affiché déja un quiz dans un autre onglet on le supprime (visuellement)
  if(state.main !== "")
  {
    document.getElementById(state.main).innerHTML = "";
  }


  // On récupère le main (dans quel onglet afficher les questions) du quiz sélectionné
  state.main = this.dataset.main;


  // On télécharge le quiz sélectionné
  getQuizEtQuestion();


  console.debug(`@clickQuiz(${state.currentQuizz})`);
  // eslint-disable-next-line no-use-before-define
}


// Fonction qui gére la demande de suppression d'une question
// Prends en paramètre id de la question
function deleteQuestionHandler(id_question) {
  // Pour éviter de surcharger index.html avec des modals
  // on réutilise un modal ici celui du login (aucune importance)
  const modal = document.getElementById('id-modal-login');


  let id;
  let trouver = false;
  let i = 0;


  // Permet d'éviter le bug d'affichage lié à l'API qui ne fait pas
  // le décallage après la suppression d'une question
  // Ici on fait le lien entre id_question fournis en paramètre
  // et le tableau des questions du quiz courant 
  while(!trouver && i < state.questions.length)
  {
    if(state.questions[i].question_id == id_question)
    {
      id = i; 
      trouver = true;
    }
    i+=1;
  }


  // On informe l'utilisateur quelle question va être supprimer
   modal.children[0].innerHTML = 
    `<blockquote>
        <h5>
          <i class="material-icons prefix small">delete_forever</i>
          <p>Voulez vous vraiment supprimer cette question ?</p>
          <p>"${state.questions[id].sentence}"</p>
        </h5>
      </blockquote>`


      // On affiche deux boutons OUI ou NON pour laisser à l'utilisateur le choix
      modal.children[1].innerHTML = 
      `<a href="#!" class="modal-close waves-effect waves-green btn-flat green" onclick=deleteQuestion(${id_question})>Oui</a>    
      <a href="#!" class="modal-close waves-effect waves-green btn-flat red">Non</a>`;    
}


// a : question_id 
// b : proposition_id
// On vérifie si le quiz que l'utilisateur regarde fait partie de la listes
// des quiz auquel l'utilisateur a déja répondu. 
// Si c'est le cas on vérifie quelle été ses réponses au quiz
function propositionsUser(a,b){
  let i = 0; // Indice première boucle
  let j = 0; // Indice seconde boucle
  let trouver = false; // Indication si le but a été atteint

  // On parcourt la liste des quiz auquel l'utilisateur a déja répondu
  while(!trouver && i < state.myanswers.length) 
  {
    // On vérifie si un des quiz ne correspond pas au quizCourant
    if(state.myanswers[i].quiz_id == state.currentQuizz)
    {
      j = 0;

      // On parcourt la liste des réponses donné par l'utilisateur lorsqu'il
      // a répondu au quiz
      while(!trouver && j < state.myanswers[i].answers.length) 
      { 

        // On regarde si le numéro de la question correspond au numéro passé en paramètre
        if(state.myanswers[i].answers[j].question_id == a)
        {

          // Maitenant on regarde si le numéro de la proposition correspond au numéro passé en paramètre
          if(state.myanswers[i].answers[j].proposition_id == b)
          {
            trouver = true;
          }

        }

        j+=1;
      }

    }

    i+=1;
  }

  // On renvoie true, on va pouvoir mettre en évidence la réponse que l'utilisateur
  // avait fournis lorsqu'il avait répondu au quiz
  // Le quiz que regarde l'utilisateur ne fait pas partie de la liste des quiz
  // on return false
  return trouver;
 }


// Quand on clique sur le bouton de login, il nous dit qui on est
// sauf si on est pas connecter, dans ce cas-là, il nous propose de nous connecter
// eslint-disable-next-line no-unused-vars
const renderUserBtn = () => {
  const btn = document.getElementById('id-login');
  const modal = document.getElementById('id-modal-login');

  btn.onclick = () => {
      // eslint-disable-next-line no-alert


    // Affichage pour la déconnexion
    if(state.user)
    {
      modal.children[0].innerHTML = 
    `<blockquote>
        <h5>
          <i class="material-icons prefix medium">account_circle</i>
          Bonjour utilisateur ${state.user.firstname}
        </h5>
      </blockquote>`

      modal.children[1].innerHTML = 
      `<a href="#!" class="modal-close waves-effect waves-green btn-flat red" onclick="logOut()"> <i class="material-icons">lock_open</i>Log Out</a>`;    
    }

    // Affichage pour la connexion
    else 
    {
      modal.children[0].innerHTML = 
      `<h5>Entrez votre login</h5>
      <form>
        <div class="row">
          <div class="input-field col s12">
            <i class="material-icons prefix">account_circle</i>
            <input id="log" type="text" class="validate">
            <label for="log">Login</label>
        </div>
      </div>
    </form>`         

      modal.children[1].innerHTML = 
      `<a href="#!" class="modal-close waves-effect waves-green btn-flat green" onclick="logIn()"> <i class="material-icons">lock_outline</i> Log In</a>`;
    }
  }
};


// Permet de se déconnecter du serveur
function logOut(){
  state.xApiKey ='';
  getMyQuizzes();
  getMyAnswers();
  getQuizzes();
  getUser();
}


// Permet de se connecter au serveur
function logIn(){
  // Si le champs où l'utilisateur rentre le login est vide
  // On ne fait rien
  if(document.getElementById("log").value !== "")
  {
    state.xApiKey = document.getElementById("log").value 

    // Important on vide le champs sinon le login reste sauvegarder dans le champs
    document.getElementById("log").value = "";

    getMyQuizzes();
    getMyAnswers();
    getQuizzes();

    if(state.main != "")
    {
      document.getElementById(state.main).innerHTML = 
      `<div class="progress">
        <div class="indeterminate"></div>
        </div>`
    } 

    if(state.currentQuiz == undefined)
    {
      getMyAnswersForRenderQuiz();
    }
    
    getUser();
  }
}

 