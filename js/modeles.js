/* globals  
            vues.js/ renderQuizzes renderUserBtn renderQuiz renderPreviewQuiz renderMyQuizzes renderMyAnswers */

// //////////////////////////////////////////////////////////////////////////////
// LE MODELE, a.k.a, ETAT GLOBAL
// //////////////////////////////////////////////////////////////////////////////

// un objet global pour encapsuler l'état de l'application
// on pourrait le stocker dans le LocalStorage par exemple
const state = {
  // la clef de l'utilisateur
  xApiKey: 'ae8dbd14-376c-4449-a0a2-8381c3f3c945',

  // l'URL du serveur où accéder aux données
  serverUrl: 'https://lifap5.univ-lyon1.fr',

  // la liste des quizzes
  quizzes: [],

  // la liste de des quizzes de l'utilisateur 
  myquizzes: [],

  // La liste des questions
  questions: [],

  // Le quiz actuel
  quiz: [],

  // L'endroit où sera afficher les information du quiz courant
  main: '',

  // la liste de des réponses de l'utilisateur 
  myanswers: [],

  // le quizz actuellement choisi
  currentQuizz: undefined,


// Une méthode pour l'objet 'state' qui va générer les headers pour les appel à fetch
header (){
  const headers = new Headers();
  headers.set('X-API-KEY', this.xApiKey);
  headers.set('Accept', 'application/json');
  headers.set('Content-Type', 'application/json');
  return headers;
  },
};

// //////////////////////////////////////////////////////////////////////////////
// OUTILS génériques
// //////////////////////////////////////////////////////////////////////////////

// un filtre simple pour récupérer les réponses HTTP qui correspondent à des
// erreurs client (4xx) ou serveur (5xx)
// eslint-disable-next-line no-unused-vars
function filterHttpResponse(response) {
  return response
    .json()
    .then((data) => {
      if (response.status >= 400 && response.status < 600) {
        throw new Error(`${data.name}: ${data.message}`);
      }
      return data;
    })
    .catch((err) => console.error(`Error on json: ${err}`));
}

// //////////////////////////////////////////////////////////////////////////////
// DONNEES DES UTILISATEURS
// //////////////////////////////////////////////////////////////////////////////

// mise-à-jour asynchrone de l'état avec les informations de l'utilisateur
// l'utilisateur est identifié via sa clef X-API-KEY lue dans l'état
// eslint-disable-next-line no-unused-vars
const getUser = () => {
    console.debug(`@getUser()`);
    const url = `${state.serverUrl}/users/whoami`;
    return fetch(url, { method: 'GET', headers: state.header() })
      .then(filterHttpResponse)
      .then((data) => {
        // /!\ ICI L'ETAT EST MODIFIE /!\
        state.user = data;
        // on lance le rendu du bouton de login
        return renderUserBtn();
      });
   
};

// //////////////////////////////////////////////////////////////////////////////
// DONNEES DES QUIZZES
// //////////////////////////////////////////////////////////////////////////////

// mise-à-jour asynchrone de l'état avec les informations de l'utilisateur
// getQuizzes télécharge la page 'p' des quizzes et la met dans l'état
// puis relance le rendu
// eslint-disable-next-line no-unused-vars
const getQuizzes = (p = 1) => {
  console.debug(`@getQuizzes(${p})`)

  const url = `${state.serverUrl}/quizzes/?page=${p}`;

  // le téléchargement est asynchrone, là màj de l'état et le rendu se fait dans le '.then'
  fetch(url, { method: 'GET', headers: state.header() })
    .then(filterHttpResponse)
    .then((data) => {
      // /!\ ICI L'ETAT EST MODIFIE /!\
      state.quizzes = data;

      // on a mis à jour les donnés, on peut relancer le rendu
      // eslint-disable-next-line no-use-before-define
      renderQuizzes();
    });

    // Dans le cas où l'utilisateur s'est déconnecté de sa session
    if(state.xApiKey === "")
    {
      if(state.main == "id-all-quizzes-main")
      {
        document.getElementById(state.main).innerHTML =
        `<div class="progress">
          <div class="indeterminate"></div>
        </div>`;
        getQuizEtQuestion();
      }
    }
};


// getQuiz télécharge le quiz courant
const getQuiz = () => {
  // Le quiz
  const url1 = `${state.serverUrl}/quizzes/${state.currentQuizz}`;
  // le téléchargement est asynchrone, là màj de l'état et le rendu se fait dans le '.then'
   fetch(url1, { method: 'GET', headers: state.header() })
    .then(filterHttpResponse)
    .then((data) => {
      // /!\ ICI L'ETAT EST MODIFIE /!\
      state.quiz = data;
      // on a mis à jour les donnés, on peut relancer le rendu
      // eslint-disable-next-line no-use-before-define
    });
}


// getQuizEtQuestions télécharge d'abord le quiz séléctionné puis les questions du quiz
// puis lance les rendu, l'apercu du quiz et de ses questions
const getQuizEtQuestion = () => {
  console.debug(`@getQuizzEtQuestion`);

  getQuiz();

  //Les questions du quiz
  const url2 = `${state.serverUrl}/quizzes/${state.currentQuizz}/questions`;
  // le téléchargement est asynchrone, là màj de l'état et le rendu se fait dans le '.then'
  fetch(url2, { method: 'GET', headers: state.header() })
    .then(filterHttpResponse)
    .then((data) => {
      // /!\ ICI L'ETAT EST MODIFIE /!\
      state.questions = data;

      // on a mis à jour les donnés, on peut relancer les rendus
      renderPreviewQuiz(); // Callback
      renderQuiz(); // Callback
      // eslint-disable-next-line no-use-before-define
    });    
};


// getMyQuizzes télécharge tous les quizzes de l'utilisateur connecté
// puis lance les rendu
const getMyQuizzes = () => {
  if(state.xApiKey !== "")
  {
    console.debug(`@getMyQuizzes()`);
    const url = "https://lifap5.univ-lyon1.fr/users/quizzes";
    fetch(url, { method: 'GET', headers: state.header() })
      .then(filterHttpResponse)
      .then((data) => {
        // /!\ ICI L'ETAT EST MODIFIE /!\
        state.myquizzes = data;

        // on a mis à jour les donnés, on peut relancer le rendu
        return renderMyQuizzes()
        // eslint-disable-next-line no-use-before-define
      });
  }

  //Si un utilisateur se déconnecte on efface ses quizzes de "Mes Quizz"
  else
  {
    const usersElt = document.getElementById('id-all-myquizzes-list').innerHTML ="";
    const main = document.getElementById("id-all-myquizzes-main").innerHTML = "";
    state.myquizzes = [];
  }    
};


// getMyAnswers télécharge tous les quizzes auquel l'utilisateur a déja répondu
// puis lance les rendu
const getMyAnswers = () => {
  if(state.xApiKey !== "")
  {
    console.debug(`@getMyAnswers()`);   
    const url = "https://lifap5.univ-lyon1.fr/users/answers";
    fetch(url, { method: 'GET', headers: state.header() })
      .then(filterHttpResponse)
      .then((data) => {
        // /!\ ICI L'ETAT EST MODIFIE /!\
        state.myanswers = data;

        // On a mis à jour les donnés, on peut relancer le rendu
        renderMyAnswers();
        // eslint-disable-next-line no-use-before-define
      });
  }

  // Si un utilisateur se déconnecte on efface ses quizzes
  else
  {
    const usersElt = document.getElementById('id-all-myanswers-list').innerHTML ="";
    const main = document.getElementById("id-all-myanswers-main").innerHTML = "";
    state.myanswers = [];
  }    
};


// getMyAnswers télécharge tous les quizzes auquel l'utilisateur a déja répondu
// puis lance renderQuiz 
// Cette fonction n'existe que pour palier au problème du renderQuiz qui a absolument
// besoin que state.myansers soit à jour avant de faire le rendu
const getMyAnswersForRenderQuiz = () => {
  if(state.xApiKey !== "")
  {
    console.debug(`@getMyAnswers()`);   
    const url = "https://lifap5.univ-lyon1.fr/users/answers";
    fetch(url, { method: 'GET', headers: state.header() })
      .then(filterHttpResponse)
      .then((data) => {
        // /!\ ICI L'ETAT EST MODIFIE /!\
        state.myanswers = data;

        // On a mis à jour les donnés, on peut relancer le rendu
        renderMyAnswers();
        getQuizEtQuestion();
        // eslint-disable-next-line no-use-before-define
      });
  }

  // Si un utilisateur se déconnecte on efface ses quizzes
  else
  {
    const usersElt = document.getElementById('id-all-myanswers-list').innerHTML ="";
    const main = document.getElementById("id-all-myanswers-main").innerHTML = "";
    state.myanswers = [];
  } 

};   

