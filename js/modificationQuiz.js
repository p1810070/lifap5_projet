/* globals state*/

//On envoie les réponses de l'utilisateur au serveur
function envoyer() {
  console.debug(`@envoyer()`);

  let reponseId; // Réponse donné par l'utilisateur
  let url; 

  let quizAnswer = state.myanswers.find(x => x.quiz_id == state.currentQuizz);
  let questionAlreadyAnswer;


  //On envoie les réponses données au serveur une à une
  for (var i = 0; i < state.quiz.questions_number ; i++) 
  { 
    for (var j = 0; j < state.questions[i].propositions_number; j++) 
    {

      if(document.getElementById(`choix${i}${j}`).checked)
      {
        reponseId = document.getElementById(`choix${i}${j}`).value;


        url = `https://lifap5.univ-lyon1.fr/quizzes/${state.currentQuizz}/questions/${i}/answers/${reponseId}`
        const envoi = () => { 
                  fetch(url, { 
                  method: 'POST', 
                  headers: state.header() 
                }).then(filterHttpResponse)
                  .then((data) => {
                    getMyAnswersForRenderQuiz();
                });
              }

        // Cas où l'utilisateur a déja répondu une fois a ce quiz
        if(quizAnswer != undefined)
        {
          questionAlreadyAnswer = quizAnswer.answers.find(y => y.question_id == i);

          // Si il a déja répondu à cette question
          if(questionAlreadyAnswer != undefined)
          {

            // Si il a fournis une réponse différente de la dernière fois
            if(reponseId != questionAlreadyAnswer.proposition_id)
            {
              envoi();
            }
          }

          // Première fois qu'il répond à cette question
          else
          {
            envoi();
          }
        }

        // Cas où l'utilisateur répond pour la première fois au quiz
        else
        {
          envoi();
        }
      }
    }
  }

    // Indication que les questions charge  
    document.getElementById(state.main).innerHTML = 
    `<div class="progress">
      <div class="indeterminate"></div>
      </div>`
}


// Permet de modifier un quiz : titre,description et si il est ouvert ou fermer
function modifyQuiz(){
  console.debug(`@modifyQuiz()`);

  let openQuiz = document.querySelector('input[name=openClose]:checked');

  // L'utilisateur n'a pas coché, on laisse l'ancienne valeur
  if(openQuiz == null)
  {
    openQuiz = state.myquizzes.find(q => q.quiz_id == state.currentQuizz).open;
  }

  else
  {
    openQuiz = document.querySelector('input[name=openClose]:checked').value;    
  }


  // On récupère le titre de la question et la description écrite dans le champ de texte
  let bodies = `{\"title\":\"${document.getElementById("titleModify").value}\",\"description\":\"${document.getElementById("descriptionModify").value}\",\"open\":\"${openQuiz}\"}`;


  const url = `https://lifap5.univ-lyon1.fr/quizzes/${state.currentQuizz}`;
  fetch(url, {
    body: bodies,
    headers: state.header() ,
    method: "PUT"
  }).then(filterHttpResponse)
      .then((data) => {
        // On ferme le formulaire après la modification
        document.getElementById("id-all-myquizzes-main").innerHTML = "";

        // On informe l'utilisateur que le quiz a été modifié
        alert("Quiz modifier");

        // eslint-disable-next-line no-use-before-define
      });
}


// On modifie UNE question : ses propositions, la question et quelle est la proposition correcte
// On passe en paramètre id de la question à modifier
function modifyQuestion(id_question){
  console.debug(`@modifyQuestion()`);


  let id;
  let trouver = false;
  let i = 0;


  // Permet d'éviter le bug d'affichage lié à l'API qui ne fait pas
  // le décallage après la suppression d'une question
  // Ici on fait le lien entre id_question fournis en paramètre
  // et le tableau des questions du quiz courant 
  while(!trouver && i < state.questions.length)
  {
    if(state.questions[i].question_id == id_question)
    {
      id = i; 
      trouver = true;
    }
    i+=1;
  }


  let nbproposition = 0;

  // La réponse à la question
  let answersQuestion = document.querySelector('input[name=reponse]:checked');

  // Si l'utilisateur a oublié de coché quelle était la réponse correcte de la question
  if(answersQuestion != null)
  { 
    // La tête du body
    let IntroBodies

    // Si l'utilisateur a supprimé la question on remet l'ancienne
    if(document.getElementById("question").value == "")
    {
      IntroBodies = `{\"sentence\":\"${state.questions[id].sentence}\",`;
    }

    else
    {
      IntroBodies = `{\"sentence\":\"${document.getElementById("question").value}\",`;
    }

    // Le corps du body
    let propositionsBodies = `\"propositions\":[`

    // On parcourt tous les champs
    for(let i = 0 ; i < state.questions[id].propositions.length ; i++)
    {

      // Si l'utilisateur a supprimé la proposition on remet l'ancienne
      if(document.getElementById(`proposition${i}`).value == "")
      {
        propositionsBodies +=`{\"content\":\"${state.questions[id].propositions[i].content}\",\"proposition_id\":${i},`
      }

      else
      {
        propositionsBodies +=`{\"content\":\"${document.getElementById(`proposition${i}`).value}\",\"proposition_id\":${i},`
      }

        // On vérifie si la proposition est la réponse à la question
        if(state.questions[id].propositions[i].proposition_id == answersQuestion.value)
        {
          propositionsBodies+=`\"correct\":true}`
        }

        else
        {
          propositionsBodies+=`\"correct\":false}` 
        }

        nbproposition++;
        propositionsBodies += `,`;
    } 

    // On enlève la dernière virgule
    propositionsBodies = propositionsBodies.substring(0, propositionsBodies.length - 1);

    propositionsBodies += `]}`;

    // On assemble la tête et le corps
    bodies = IntroBodies+propositionsBodies;

    // Pour envoyer au serveur, il faut avoir remplie le champs question et proposé au 
    // moins une proposition
    const url = `https://lifap5.univ-lyon1.fr/quizzes/${state.currentQuizz}/questions/${id_question}/`;

    fetch(url, {
      body: bodies,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-Api-Key": "ae8dbd14-376c-4449-a0a2-8381c3f3c945"
      },
      method: "PUT"
    }).then(filterHttpResponse)
          .then((data) => {
          getQuizEtQuestion();
          alert("Question modifiée");
          })
  }

  else
  {
    alert("Vous avez oublié de selectionné la bonne réponse")
  }

}



