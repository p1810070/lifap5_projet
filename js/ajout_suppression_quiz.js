/* globals state*/

// On créer un quiz
function creationQuiz() {
  console.debug(`@creationQuiz()`);

  // On récupère le titre de la question et la description écrite dans le champ de texte
  let bodies = `{\"title\":\"${document.getElementById("title").value}\",\"description\":\"${document.getElementById("description").value}\"}`;

  const url = `https://lifap5.univ-lyon1.fr/quizzes/`;

  fetch(url, {
    body: bodies,
    headers: state.header() ,
    method: "POST"
  }).then(filterHttpResponse)
      .then((data) => {
        alert("Quiz créer");

        // L'utilisateur a fini on efface le formulaire de création
        document.getElementById("id-all-myquizzes-main").innerHTML = "";

        // eslint-disable-next-line no-use-before-define
      });
}


// Gére l'ajout d'UNE question à un quiz
function addQuestionToQuiz() {
  console.debug(`@addQuestionToQuiz()`);

  let nbproposition = 0;

  // La réponse à la question
  let answersQuestion = document.querySelector('input[name=reponse]:checked');


  if(document.getElementById("question").value != "")
  {
    if(answersQuestion != null)
    {
      // La tête du body
      let IntroBodies = `{\"question_id\":${state.quiz.questions_number},\"sentence\":\"${document.getElementById("question").value}\",
      \"propositions\":`;


      // Le corps du body
      let propositionsBodies = `[`

      // On parcourt tous les champs propositions
      for(let i = 0 ; i < 4 ; i++)
      {

        // On récupère tous les champs non-vide
        if(document.getElementById(`proposition${i}`).value != "")
        {
          propositionsBodies += `{\"content\":\"${document.getElementById(`proposition${i}`).value}\",\"proposition_id\":${i},`;

          // On vérifie si la proposition est la réponse à la question
          if(i == answersQuestion.value)
          {
            propositionsBodies += `\"correct\":true}` 
          }
          else
          {
            propositionsBodies += `\"correct\":false}`
          }

          nbproposition++;
          propositionsBodies += `,`;
        }
      }

      // On enlève la dernière virgule
      propositionsBodies = propositionsBodies.substring(0, propositionsBodies.length - 1);

      propositionsBodies += `]}`;

      // On assemble la tête et le corps
      bodies = IntroBodies+propositionsBodies;

      // Pour envoyer au serveur, il faut avoir remplie le champs question, avoir proposé au 
      // moins une proposition et ne pas avoir mis pour réponse correcte une proposition non saisie
      if(nbproposition > 0  && document.getElementById(`proposition${answersQuestion.value}`).value !="")
      {
        const url = `https://lifap5.univ-lyon1.fr/quizzes/${state.currentQuizz}/questions/`;
        fetch(url, {
          body: bodies,
          headers: state.header() ,
          method: "POST"
        }).then(filterHttpResponse)
            .then((data) => {
              alert("Question créer")

              getQuizEtQuestion();
              // eslint-disable-next-line no-use-before-define
            });
      }

      else
      {
        alert("Saisissez au moins une proposition et séléctionnez la proposition qui est correcte");
      }

    }

    else
    {
      alert("Vous n'avez pas séléctionné la proposition qui correspond à la bonne réponse");
    }

  }

  else
  {
    alert("Manque la question");
  }
}


// On supprime UNE question
// On passe en paramètre id de la question à supprimer
function deleteQuestion(id){
  console.debug(`@DeleteQuestion()`);

  const url = `https://lifap5.univ-lyon1.fr/quizzes/${state.currentQuizz}/questions/${id}/`

  fetch(url,{
    headers: state.header(),
    method: "DELETE" 
  }).then(filterHttpResponse)
        .then((data) => {
          
       getQuizEtQuestion();
  })
}