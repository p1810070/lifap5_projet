/* global modeles.js/ state */


// Génére le formulaire pour ajouter un quiz
function clickAdd(){
  // On affiche le formulaire dans l'onglet "Mes Quiz"
  const main = document.getElementById("id-all-myquizzes-main");


  // On affiche le formulaire de création de quiz
  main.innerHTML = 
  `
  <h4>Création d'un quiz</h4>
  </br>
  <div class="row">
    <form class="col s12">
      <div class="row">
        <div class="input-field col s12">
          <input id="title" type="text" class="validate">
          <label for="title">Titre</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input id="description" type="text" class="validate">
          <label for="description">Description</label>
        </div>
      </div>
      <button class="btn waves-effect waves-light right" type="button" name="createNewQuiz" onclick= creationQuiz()>Créer
      <i class="material-icons right">send</i>
      </button>
    </form>
  </div>`        
}


// Génére le formulaire pour ajouter une questions
function clickAddQuestion(){
  // On affiche le formulaire dans l'onglet "Mes Quiz"
  const main = document.getElementById("id-all-myquizzes-main");


  // On récupère les questions déja existante
  const questionList = state.questions
    .map(
    (q1) =>`
      <li>
        <h5>${q1.sentence}</h5>
        <ol>
          ${q1.propositions.map((q2) =>` 
            <li>   
              <p>
                <label>
                    <input type="radio" id="choix${q1.question_id}${q2.proposition_id}" name="${q1.question_id}" value="${q2.proposition_id}"/>
                    <span>${q2.content}</span>
                </label>
              </p>
            </li>`)
          .join('')}
      </ol>
    </li>`)
  .join('');


  // On génère le formulaire pour ajouter la/les proposition(s)
  const formContent = 
      `<div class="row">
        <div class="input-field col s12">
          <input id="question" type="text" class="validate">
          <label for="question">Question</label>
        </div>
      </div>
      <div class="row" id="champsProposition">
        <div class="input-field col s12">
          <input id="proposition0" type="text" class="validate">
          <label for="proposition0">proposition 1</label>
        </div>
      </div>
  </div>
  <p>
    <label>
      <input type="radio" id="proposition0" name="reponse" value="0"/>
      <span>La proposition 1 est la bonne réponse</span>
    </label>
  </p>
   <div class="row" id="champsProposition">
        <div class="input-field col s12">
          <input id="proposition1" type="text" class="validate">
          <label for="proposition1">proposition 2</label>
        </div>
      </div>
  </div>
  <p>
    <label>
      <input type="radio" id="proposition1" name="reponse" value="1"/>
      <span>La proposition 2 est la bonne réponse</span>
    </label>
  </p>
   <div class="row" id="champsProposition">
        <div class="input-field col s12">
          <input id="proposition2" type="text" class="validate">
          <label for="proposition2">proposition 3</label>
        </div>
      </div>
  </div>
  <p>
    <label>
      <input type="radio" id="proposition2" name="reponse" value="2"/>
      <span>La proposition 3 est la bonne réponse</span>
    </label>
  </p>
  <div class="row" id="champsProposition">
        <div class="input-field col s12">
          <input id="proposition3" type="text" class="validate">
          <label for="proposition3">proposition 4</label>
      </div>
  </div>
  <p>
    <label>
      <input type="radio" id="proposition3" name="reponse" value="3"/>
      <span>La proposition 4 est la bonne réponse</span>
    </label>
  </p>
  </div>
  `  

  // On affiche le formulaire de création de question
  main.innerHTML = 
  `<h4>Ajout d'une nouvelle question</h4>
  </br>

  <ul>
    ${questionList}
  </ul>

  </br>
  <p>Nouvelle question<p>

  <form class="col s12">
    ${formContent}

    <!-- Bouton pour confirmer la création du quiz -->
    <button class="btn waves-effect waves-light right" type="button" name="addNewQuestion" onclick= addQuestionToQuiz()>Ajouter
      <i class="material-icons right">add</i>
    </button>
  </form>`  
        
}


// Fonction qui affiche le formulaire de modification d'un quiz
// Titre et description
// Prend en paramètre id du quiz à modifier
function clickModifyQuiz(id) {
  // On affiche le formulaire dans l'onglet "Mes Quiz"
  const main = document.getElementById("id-all-myquizzes-main");


  // On récupère l'id du quiz sélectionné
  state.currentQuizz = id;

  console.debug(`${state.currentQuizz}`);


  // On affiche le formulaire de modification du quiz (déja pré-rempli)
  main.innerHTML = 
  `
  <h4>Modification d'un quiz</h4>
  </br>
  <div class="row">
    <form class="col s12">
      <div class="row">
        <div class="input-field col s12">
          <input value="${state.myquizzes.find(q => q.quiz_id == state.currentQuizz).title}" id="titleModify" type="text" class="validate">
          <label class="active" for="titleModify">Titre</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input  value="${state.myquizzes.find(q => q.quiz_id == state.currentQuizz).description}" id="descriptionModify" type="text" class="validate">
          <label class="active" for="descriptionModify">Description</label>
        </div>
      </div>
      <p>Etat du quiz : ${state.myquizzes.find(q => q.quiz_id == state.currentQuizz).open?"Ouvert":"Fermer"} </p>
      <ul>
        <li>   
          <p>
            <label>
                <input type="radio" id="OPEN" name="openClose" value="true"/>
                <span>Ouvrir le quiz</span>
            </label>
          </p>
            <label>
                <input type="radio" id="CLOSE" name="openClose" value="false"/>
                <span>Fermer le quiz</span>
            </label>            
          </p>
        </li>
      </ul>
      <button class="btn waves-effect waves-light right" type="button" name="modify" onclick=modifyQuiz()>Modifier
      <i class="material-icons right">send</i>
      </button>
    </form>
  </div>` 

}


// Affiche un formulaire qui permet de modifier la question
// d'un quiz et ses propositions
// La fonction prend en paramètre l'id de la question
function clickModifyQuestion(id_question) {
  // On affiche le formulaire dans l'onglet "Mes Quiz"
  const main = document.getElementById("id-all-myquizzes-main");

  let id;
  let trouver = false;
  let i = 0;


  // Permet d'éviter le bug d'affichage lié à l'API qui ne fait pas
  // le décallage après la suppression d'une question
  // Ici on fait le lien entre id_question fournis en paramètre
  // et le tableau des questions du quiz courant 
  while(!trouver && i < state.questions.length)
  {
    if(state.questions[i].question_id == id_question)
    {
      id = i; 
      trouver = true;
    }
    i+=1;
  }

  // On génère le formulaire
  let formContent = "";


    // La question
    formContent+=
    `<div class="row">
        <div class="input-field col s12">
          <input value="${state.questions[id].sentence}" id="question" type="text" class="validate">
          <label class="active" for="question">Question</label>
        </div>
      </div>`;


   // On parcourt l'ensemble des propositions de la question
   // On affiche le formulaire de modification d'une question (déja pré-rempli)
  for(let i = 0 ; i < state.questions[id].propositions.length ; i++)
  { 
    formContent+=
    `<div class="row" id="champsProposition">
        <div class="input-field col s12">
            <input value="${state.questions[id].propositions[i].content}" id="proposition${i}" type="text" class="validate">
            <label class="active" for="proposition${i+1}">proposition ${i+1}</label>
          </div>
        </div>
    </div>`;
  }


  let formAnswers ="La bonne proposition est : ";

  // On créer des boutons radio pour permettre à l'utilisateur de préciser 
  // quelles propositions est juste
  for(let j = 0 ; j < state.questions[id].propositions.length ; j++)
  {
    formAnswers+=
    `<li>   
      <p>
        <label>
            <input type="radio" id="reponse${state.questions[id].propositions[j].proposition_id}" name="reponse" value="${state.questions[id].propositions[j].proposition_id}"/>
            <span>Proposition ${j+1}</span>
        </label>
      </p>
    </li>`;
  }


  // On affiche le formulaire de modification de question
  main.innerHTML = 
  `<h4>Modification d'une question</h4>
  </br>
  </br>
  <form class="col s12">
    ${formContent}
    <ul>
      ${formAnswers}
    </ul>

    <!-- Bouton pour confirmer la modification -->
    <button class="btn waves-effect waves-light right" type="button" name="modify" onclick= modifyQuestion(${id_question})>Modifier
      <i class="material-icons right">send</i>
    </button>
  </form>`  
}
